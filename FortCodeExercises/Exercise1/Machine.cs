namespace FortCodeExercises.Exercise1
{
    public class Machine
    {
        public int Type = 0;

        public string Name => Type switch
        {
            0 => "bulldozer",
            1 => "crane",
            2 => "tractor",
            3 => "truck",
            4 => "car",
            _ => ""

        };

        public string Description
        {
            get
            {
                string GetDescription(int speed)
                {
                    return $" {Color} {Name} [{speed}].";
                }

                return Type switch
                {
                    0 => GetDescription(GetMaxSpeed(Type, true)),
                    1 => GetDescription(GetMaxSpeed(Type, true)),
                    2 => GetDescription(GetMaxSpeed(Type, true)),
                    3 => GetDescription(GetMaxSpeed(Type)),
                    4 => GetDescription(GetMaxSpeed(Type)),
                    _ => ""
                };
            }
        }

        public string Color =>
            Type switch
            {
                0 => "red",
                1 => "blue",
                2 => "green",
                3 => "yellow",
                4 => "brown",
                _ => "white",

            };
        public string TrimColor =>
            !IsDark(Color) ? Type switch
            {
                1 => "white",
                _ => ""
            } : Type switch
            {
                1 => "black",
                2 => "gold",
                3 => "silver",
                _ => ""
            };

        public bool IsDark(string color) => color switch
        {
            "red" => true,
            "green" => true,
            "crimson" => true,
            "black" => true,
            _ => false
        };

        public int GetMaxSpeed(int machineType, bool noMax = false) => machineType switch
        {
            1 when noMax == false => 70,
            2 when noMax == false => 60,
            0 when noMax => 80,
            1 => 75,
            2 => 90,
            4 when noMax => 90,
            _ => 70
        };
    }
}