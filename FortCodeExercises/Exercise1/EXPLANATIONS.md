# Refactoring

### Naming Convention: 
The class default naming convention of the properties to camel casing, refactored to use PascalCase for .net compatibility

### Immutability
The Type property has a public getter and setter is not advisable to have setter as the object behaviour can be modified at any time of the object lifecycle. 
Recommended to use constructor and pass the type into the Machine class and remove the default constructor

### Big Refactor
All the logic can be extracted into MachineDetailsParser.cs class and create this object in the machine class constructor and move all the business logic into it. The Setters on the machine class should be done in the constructor from the newly created object. Below is an example.

```
public class Machine  {

    public Machine(int type)
    {
        var details = new MachineDetailsParser(type);
        Description = details.GetDescription();
        Color = details.GetColor();
        ...
    }
    public string Color {get; private set;}
    public string Description {get; private set;}
    ...
}

```
