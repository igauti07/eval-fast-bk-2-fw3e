﻿using System;
using FluentAssertions;
using FortCodeExercises.Exercise1;
using NUnit.Framework;

namespace FortCodeExercises.Tests
{

    public class MachineTests
    {
        [TestCaseSource(nameof(_testInput))]
        public void Validate((string, int, string, string, string, string) inputMappings)
        {
            var (_, type, machineName, description, color, trimColor) = inputMappings;
            var subject = new Machine
            {
                Type = type
            };
            subject.Name.Should().BeEquivalentTo(machineName);
            subject.Description.Should().BeEquivalentTo(description);
            subject.Color.Should().BeEquivalentTo(color);
            subject.TrimColor.Should().BeEquivalentTo(trimColor);
        }


        [TestCase("red", true)]
        [TestCase("yellow", false)]
        [TestCase("green", true)]
        [TestCase("black", true)]
        [TestCase("white", false)]
        [TestCase("beige", false)]
        [TestCase("babyblue", false)]
        [TestCase("crimson", true)]
        public void isDarkTests(string color, bool expected)
        {
            var subject = new Machine();
            subject.IsDark(color).Should().Be(expected);
        }

        [TestCase(0, 70)]
        [TestCase(1, 70)]
        [TestCase(2, 60)]
        [TestCase(3, 70)]
        [TestCase(4, 70)]
        public void getMaxSpeedTestsForNoMax(int type, int expected)
        {
            var subject = new Machine();
            subject.GetMaxSpeed(type).Should().Be(expected);
        }

        [TestCase(0, 80)]
        [TestCase(1, 75)]
        [TestCase(2, 90)]
        [TestCase(3, 70)]
        [TestCase(4, 90)]
        public void getMaxSpeedTestsForNoMaxTrue(int type, int expected)
        {
            var subject = new Machine();
            subject.GetMaxSpeed(type, true).Should().Be(expected);
        }

        private static (string, int, string, string, string, string)[] _testInput =
        {
            ("Type:0", 0, "bulldozer", " red bulldozer [80].", "red", ""),
            ("Type:1", 1, "crane",  " blue crane [75].", "blue", "white"),
            ("Type:2", 2, "tractor", " green tractor [90].", "green",  "gold"),
            ("Type:3", 3, "truck", " yellow truck [70].", "yellow", ""),
            ("Type:4", 4, "car", " brown car [70].", "brown", "")
        };
    }
}
